#!/bin/sh

if [ "$(id -u)" -eq 0 ]
then
  echo "This script should not be executed by root user."
  exit 1
fi

#==============================================================================#
#------------------------------------------------------------------------------#
# Change default background

gsettings set org.gnome.desktop.background picture-uri ""
gsettings set org.gnome.desktop.background primary-color "#383f47"
gsettings set org.gnome.desktop.background secondary-color "#383f47"
gsettings set org.gnome.desktop.background color-shading-type "solid"

#==============================================================================#
#------------------------------------------------------------------------------#
# Change to dark theme

gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"

#==============================================================================#
#------------------------------------------------------------------------------#
# Do not prompt on logout

#gsettings set org.gnome.gnome-session logout-prompt false

#==============================================================================#
#------------------------------------------------------------------------------#
# Disable Animation

gsettings set org.gnome.desktop.interface enable-animations false

#==============================================================================#
#------------------------------------------------------------------------------#
# Activate compose key
#

gsettings set org.gnome.desktop.input-sources xkb-options "['compose:ralt']"

#==============================================================================#
#------------------------------------------------------------------------------#
# Inverse Caps Lock and Escape Key

gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"


#==============================================================================#
#------------------------------------------------------------------------------#
### https://wecros.com/2020/09/19/how-to-change-shortcuts-in-gnome-desktop-environment/
# Set Shortcut

# remove the window key behavior
gsettings set org.gnome.mutter overlay-key ""


# Browser:					win + `
# Terminal:					win + enter
# Nautilus:					win + e
# Rofi:						win + space

# win max:					win + j
# win min:					win + k
# win tile left:				win + h
# win tile right:				win + l
# win kill:					ctrl + shift + q

# switch to workspace:				win + [1,2,3,4,5,6...]
# send win to workspace:			win + shift + [1,2,3,4,5,6...]

# toggle mute sound:				win + 0
# sound up:					win + =
# sound down:					win + -

# show all window:				win + shift + espace

##### KEYBINDING SETUP HERE

gsettings set org.gnome.settings-daemon.plugins.media-keys screensaver "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-group @as "[]"
gsettings set org.gnome.desktop.wm.keybindings begin-resize "['<Alt>F8']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings begin-move "['<Alt>F7']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-w @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-corner-nw @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-10 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-6 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "['<Control><Shift><Alt>Right']"
gsettings set org.gnome.desktop.wm.keybindings always-on-top @as "[]"
gsettings set org.gnome.desktop.wm.keybindings toggle-maximized "['<Super>m']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left "['<Control><Shift><Alt>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings cycle-panels @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-11 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings lower @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-7 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings toggle-above @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down "['<Super><Shift>Page_Down', '<Control><Shift><Alt>Down']"
gsettings set org.gnome.desktop.wm.keybindings switch-panels @as "[]"
gsettings set org.gnome.desktop.wm.keybindings minimize @as "[]"
gsettings set org.gnome.desktop.wm.keybindings cycle-windows "['<Alt>Escape']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-12 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings toggle-on-all-workspaces @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-input-source @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-8 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-n @as "[]"
gsettings set org.gnome.desktop.wm.keybindings maximize-horizontally @as "[]"
gsettings set org.gnome.desktop.wm.keybindings activate-window-menu "['<Alt>space']"
gsettings set org.gnome.desktop.wm.keybindings set-spew-mark @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-windows-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings maximize-vertically @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-corner-sw @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-9 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>k']"
gsettings set org.gnome.desktop.wm.keybindings panel-main-menu "['<Alt>F1']"
gsettings set org.gnome.desktop.wm.keybindings close "['<Shift><Super>q']"
gsettings set org.gnome.desktop.wm.keybindings move-to-monitor-up "['<Super><Shift>Up']"
gsettings set org.gnome.desktop.wm.keybindings raise-or-lower @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-e @as "[]"
gsettings set org.gnome.desktop.wm.keybindings cycle-windows-backward "['<Shift><Alt>Escape']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Super>1']"
gsettings set org.gnome.desktop.wm.keybindings move-to-monitor-right "['<Super><Shift>Right']"
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Super>Tab', '<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings panel-run-dialog "['<Alt>F2']"
gsettings set org.gnome.desktop.wm.keybindings switch-panels-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Super>j']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Super>2']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "['<Super><Shift>space']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-last @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-1 "['<Shift><Super>exclam']"
gsettings set org.gnome.desktop.wm.keybindings move-to-corner-ne @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Super>3']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Super>Page_Up', '<Control><Alt>Up']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-s @as "[]"
gsettings set org.gnome.desktop.wm.keybindings show-desktop @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-center @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-2 "['<Shift><Super>at']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['<Control><Alt>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Control><Alt>Right']"
gsettings set org.gnome.desktop.wm.keybindings raise @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-corner-se @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-10 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Super>4']"
gsettings set org.gnome.desktop.wm.keybindings toggle-shaded @as "[]"
gsettings set org.gnome.desktop.wm.keybindings cycle-group-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-3 "['<Shift><Super>numbersign']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Super>Page_Down', '<Control><Alt>Down']"
gsettings set org.gnome.desktop.wm.keybindings cycle-panels-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-monitor-left "['<Super><Shift>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications-backward "['<Super>space']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-11 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-input-source-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-4 "['<Shift><Super>dollar']"
gsettings set org.gnome.desktop.wm.keybindings move-to-monitor-down "['<Super><Shift>Down']"
gsettings set org.gnome.desktop.wm.keybindings toggle-fullscreen @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-12 @as "[]"
gsettings set org.gnome.desktop.wm.keybindings cycle-group @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up "['<Super><Shift>Page_Up', '<Control><Shift><Alt>Up']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-last "['<Super><Shift>End']"
gsettings set org.gnome.desktop.wm.keybindings switch-group-backward @as "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-5 @as "[]"
gsettings set org.freedesktop.ibus.engine.typing-booster keybindings "{'cancel': <['Escape']>, 'commit_candidate_1': <@as []>, 'commit_candidate_1_plus_space': <['1', 'KP_1', 'F1']>, 'commit_candidate_2': <@as []>, 'commit_candidate_2_plus_space': <['2', 'KP_2', 'F2']>, 'commit_candidate_3': <@as []>, 'commit_candidate_3_plus_space': <['3', 'KP_3', 'F3']>, 'commit_candidate_4': <@as []>, 'commit_candidate_4_plus_space': <['4', 'KP_4', 'F4']>, 'commit_candidate_5': <@as []>, 'commit_candidate_5_plus_space': <['5', 'KP_5', 'F5']>, 'commit_candidate_6': <@as []>, 'commit_candidate_6_plus_space': <['6', 'KP_6', 'F6']>, 'commit_candidate_7': <@as []>, 'commit_candidate_7_plus_space': <['7', 'KP_7', 'F7']>, 'commit_candidate_8': <@as []>, 'commit_candidate_8_plus_space': <['8', 'KP_8', 'F8']>, 'commit_candidate_9': <@as []>, 'commit_candidate_9_plus_space': <['9', 'KP_9', 'F9']>, 'enable_lookup': <['Tab', 'ISO_Left_Tab']>, 'lookup_related': <['Mod5+F12']>, 'lookup_table_page_up': <['Page_Up', 'KP_Page_Up', 'KP_Prior']>, 'lookup_table_page_down': <['Page_Down', 'KP_Page_Down', 'KP_Next']>, 'next_case_mode': <['Shift_L']>, 'next_dictionary': <['Mod1+Down', 'Mod1+KP_Down']>, 'next_input_method': <['Control+Down', 'Control+KP_Down']>, 'previous_case_mode': <['Shift_R']>, 'previous_input_method': <['Control+Up', 'Control+KP_Up']>, 'previous_dictionary': <['Mod1+Up', 'Mod1+KP_Up']>, 'remove_candidate_1': <['Control+1', 'Control+KP_1', 'Control+F1']>, 'remove_candidate_2': <['Control+2', 'Control+KP_2', 'Control+F2']>, 'remove_candidate_3': <['Control+3', 'Control+KP_3', 'Control+F3']>, 'remove_candidate_4': <['Control+4', 'Control+KP_4', 'Control+F4']>, 'remove_candidate_5': <['Control+5', 'Control+KP_5', 'Control+F5']>, 'remove_candidate_6': <['Control+6', 'Control+KP_6', 'Control+F6']>, 'remove_candidate_7': <['Control+7', 'Control+KP_7', 'Control+F7']>, 'remove_candidate_8': <['Control+8', 'Control+KP_8', 'Control+F8']>, 'remove_candidate_9': <['Control+9', 'Control+KP_9', 'Control+F9']>, 'select_next_candidate': <['Tab', 'ISO_Left_Tab', 'Down', 'KP_Down']>, 'select_previous_candidate': <['Shift+Tab', 'Shift+ISO_Left_Tab', 'Up', 'KP_Up']>, 'setup': <['Mod5+F10']>, 'speech_recognition': <@as []>, 'toggle_emoji_prediction': <['Mod5+F6']>, 'toggle_off_the_record': <['Mod5+F9']>, 'toggle_hide_input': <['Shift+Control+Escape']>, 'toggle_input_mode_on_off': <@as []>}"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-4 "['<Primary><Alt>F4']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-10 "['<Primary><Alt>F10']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-3 "['<Primary><Alt>F3']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-9 "['<Primary><Alt>F9']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-2 "['<Primary><Alt>F2']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-8 "['<Primary><Alt>F8']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-1 "['<Primary><Alt>F1']"
gsettings set org.gnome.mutter.wayland.keybindings restore-shortcuts @as "[]"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-7 "['<Primary><Alt>F7']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-12 "['<Primary><Alt>F12']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-6 "['<Primary><Alt>F6']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-11 "['<Primary><Alt>F11']"
gsettings set org.gnome.mutter.wayland.keybindings switch-to-session-5 "['<Primary><Alt>F5']"
gsettings set org.gnome.shell.keybindings switch-to-application-1 "['<Super>1']"
gsettings set org.gnome.shell.keybindings switch-to-application-7 "['<Super>7']"
gsettings set org.gnome.shell.keybindings toggle-message-tray "['<Shift><Super>asciitilde']"
gsettings set org.gnome.shell.keybindings switch-to-application-6 "['<Super>6']"
gsettings set org.gnome.shell.keybindings open-application-menu "['<Super>F10']"
gsettings set org.gnome.shell.keybindings switch-to-application-5 "['<Super>5']"
gsettings set org.gnome.shell.keybindings toggle-application-view @as "[]"
gsettings set org.gnome.shell.keybindings switch-to-application-4 "['<Super>4']"
gsettings set org.gnome.shell.keybindings focus-active-notification "['<Super>n']"
gsettings set org.gnome.shell.keybindings switch-to-application-3 "['<Super>3']"
gsettings set org.gnome.shell.keybindings switch-to-application-9 "['<Super>9']"
gsettings set org.gnome.shell.keybindings switch-to-application-2 "['<Super>2']"
gsettings set org.gnome.shell.keybindings switch-to-application-8 "['<Super>8']"
gsettings set org.gnome.shell.keybindings toggle-overview @as "[]"
gsettings set org.gnome.mutter.keybindings tab-popup-cancel @as "[]"
gsettings set org.gnome.mutter.keybindings tab-popup-select @as "[]"
gsettings set org.gnome.mutter.keybindings toggle-tiled-right "['<Super>l']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-left "['<Super>h']"
gsettings set org.gnome.mutter.keybindings rotate-monitor "['XF86RotateWindows']"
gsettings set org.gnome.mutter.keybindings switch-monitor "['<Super>p', 'XF86Display']"

gsettings set org.gnome.settings-daemon.plugins.media-keys media "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys stop-static "['XF86AudioStop']"
gsettings set org.gnome.settings-daemon.plugins.media-keys eject-static "['XF86Eject']"
gsettings set org.gnome.settings-daemon.plugins.media-keys rotate-video-lock "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-cycle "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys toggle-contrast "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys rotate-video-lock-static "['<Super>o', 'XF86RotationLockToggle']"
gsettings set org.gnome.settings-daemon.plugins.media-keys www "['<Super>grave']"
gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot-clip "['<Ctrl><Alt>Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys battery-status-static "['XF86Battery']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down "['<Super>minus']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-repeat "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys hibernate "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down-precise "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys next "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys suspend "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-toggle-static "['XF86TouchpadToggle', '<Ctrl><Super>XF86TouchpadToggle']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up-quiet "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-up-static "['XF86MonBrightnessUp']"
gsettings set org.gnome.settings-daemon.plugins.media-keys play-static "['XF86AudioPlay', '<Ctrl>XF86AudioPlay']"
gsettings set org.gnome.settings-daemon.plugins.media-keys search-static "['XF86Search']"
gsettings set org.gnome.settings-daemon.plugins.media-keys magnifier-zoom-in "['<Alt><Super>equal']"
gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot-clip "['<Ctrl><Shift>Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys mic-mute "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys stop "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys previous "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up "['<Super>equal']"
gsettings set org.gnome.settings-daemon.plugins.media-keys control-center "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys search "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys calculator "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-step 6
gsettings set org.gnome.settings-daemon.plugins.media-keys rfkill-static "['XF86WLAN', 'XF86UWB', 'XF86RFKill']"
gsettings set org.gnome.settings-daemon.plugins.media-keys pause-static "['XF86AudioPause']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-mute-static "['XF86AudioMute']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up-static "['XF86AudioRaiseVolume', '<Ctrl>XF86AudioRaiseVolume']"
gsettings set org.gnome.settings-daemon.plugins.media-keys calculator-static "['XF86Calculator']"
gsettings set org.gnome.settings-daemon.plugins.media-keys home-static "['XF86Explorer']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-on "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys www-static "['XF86WWW']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-mute-quiet-static "['<Alt>XF86AudioMute']"
gsettings set org.gnome.settings-daemon.plugins.media-keys magnifier-zoom-out "['<Alt><Super>minus']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down-quiet-static "['<Alt>XF86AudioLowerVolume', '<Alt><Ctrl>XF86AudioLowerVolume']"
gsettings set org.gnome.settings-daemon.plugins.media-keys window-screenshot "['<Alt>Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys power "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys play "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys max-screencast-length uint32 30
gsettings set org.gnome.settings-daemon.plugins.media-keys power-static "['XF86PowerOff']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down-static "['XF86AudioLowerVolume', '<Ctrl>XF86AudioLowerVolume']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-up "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-forward "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-random-static "['XF86AudioRandomPlay']"
gsettings set org.gnome.settings-daemon.plugins.media-keys pause "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-cycle-static "['XF86MonBrightnessCycle']"
gsettings set org.gnome.settings-daemon.plugins.media-keys media-static "['XF86AudioMedia']"
gsettings set org.gnome.settings-daemon.plugins.media-keys hibernate-static "['XF86Suspend', 'XF86Hibernate']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot-clip "['<Ctrl>Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys eject "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys email-static "['XF86Mail']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-up "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys rfkill-bluetooth "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-toggle "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-toggle "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys logout "['<Control><Alt>Delete']"
gsettings set org.gnome.settings-daemon.plugins.media-keys help "['', '<Super>F1']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-random "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot "['<Shift>Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys decrease-text-size "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up-quiet-static "['<Alt>XF86AudioRaiseVolume', '<Alt><Ctrl>XF86AudioRaiseVolume']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screencast @as "[]"
gsettings set org.gnome.settings-daemon.plugins.media-keys screensaver-static "['XF86ScreenSaver']"
gsettings set org.gnome.settings-daemon.plugins.media-keys email "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down-quiet "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-down-static "['XF86MonBrightnessDown']"
gsettings set org.gnome.settings-daemon.plugins.media-keys increase-text-size "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-off-static "['XF86TouchpadOff']"
gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>e']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-rewind-static "['XF86AudioRewind']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screenreader "['<Alt><Super>s']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-repeat-static "['XF86AudioRepeat']"
gsettings set org.gnome.settings-daemon.plugins.media-keys magnifier "['<Alt><Super>8']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up-precise "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-rewind "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys suspend-static "['XF86Sleep']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-mute "['<Super>0']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-toggle-static "['XF86KbdLightOnOff']"
gsettings set org.gnome.settings-daemon.plugins.media-keys mic-mute-static "['XF86AudioMicMute']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-up-precise-static "['<Shift>XF86AudioRaiseVolume', '<Ctrl><Shift>XF86AudioRaiseVolume']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-up-static "['XF86KbdBrightnessUp']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screen-brightness-down "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys rfkill "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys playback-forward-static "['XF86AudioForward']"
gsettings set org.gnome.settings-daemon.plugins.media-keys rfkill-bluetooth-static "['XF86Bluetooth']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-down "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screensaver "['<Super>x']"
gsettings set org.gnome.settings-daemon.plugins.media-keys on-screen-keyboard "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys control-center-static "['XF86Tools']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-mute-quiet "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-off "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "['Print']"
gsettings set org.gnome.settings-daemon.plugins.media-keys battery-status "['']"
gsettings set org.gnome.settings-daemon.plugins.media-keys touchpad-on-static "['XF86TouchpadOn']"
gsettings set org.gnome.settings-daemon.plugins.media-keys next-static "['XF86AudioNext', '<Ctrl>XF86AudioNext']"
gsettings set org.gnome.settings-daemon.plugins.media-keys previous-static "['XF86AudioPrev', '<Ctrl>XF86AudioPrev']"
gsettings set org.gnome.settings-daemon.plugins.media-keys keyboard-brightness-down-static "['XF86KbdBrightnessDown']"
gsettings set org.gnome.settings-daemon.plugins.media-keys volume-down-precise-static "['<Shift>XF86AudioLowerVolume', '<Ctrl><Shift>XF86AudioLowerVolume']"
