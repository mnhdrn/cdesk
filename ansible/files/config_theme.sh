#!/bin/sh

if [ "$(id -u)" -ne 0 ]
then
  echo "This script should be executed by root user"
  exit 1
fi

#==============================================================================#
#------------------------------------------------------------------------------#
# Edit Gnome-Shell theme

cp /usr/share/gnome-shell/theme/gnome-classic.css /usr/share/gnome-shell/theme/gnome-classic.css.old
sed -i "s/.*gradient.*//g" /usr/share/gnome-shell/theme/gnome-classic.css
sed -i "s/color:.*;/color: #ececec;/g" /usr/share/gnome-shell/theme/gnome-classic.css
sed -i "s/background-color:.*;/background-color: #2d2d2d;/g" /usr/share/gnome-shell/theme/gnome-classic.css
sed -i "s/border-top-color:.*;/border-top-color: #2d2d2d;/g" /usr/share/gnome-shell/theme/gnome-classic.css
sed -i "s/-arrow-border-color:.*;/-arrow-border-color: #2d2d2d;/g" /usr/share/gnome-shell/theme/gnome-classic.css
sed -i "s/#4b92e7/#2d2d2d/g" /usr/share/gnome-shell/theme/gnome-classic.css
