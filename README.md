# cdesk

A classic Gnome Classic Desktop Customisation

### Usage

install Git and Ansible

run the main.yml playbook from ansible directory like that:

```
ansible-playbook -k -t all main.yml
```

**Playbook Action:**
- install package present in packages/packages.list
- install rpm fusion repository and some codec from packages/packages_codec.yml
- copy dotfiles
- configure shortcut
- activate compose key on _Right Alt Key_
- swap Caps Lock key with Escape Key (Intensive Vim user attended)
- set theme to "Adwaita Dark"
- remove animation
- customise Gnome classic panel with the script in ansible/files/config_theme.sh, so it match with "Adwaita Dark"
- change default shell to a zsh with a custom theme & configuration
- User NeoVim by default with my own configuration

**Todo:**
- add a terminal profile customisation for gnome terminal
- Continue shortcut configuration
- a plays file to add my custom image (Profile Image & Desktop Background)

### Shortcut

- open Brave Browser:			win + '`'					:x:
- open Terminal:				win + 'enter'				:x:
- open Nautilus:				win + 'e'					:heavy_check_mark:
- open Rofi:					win + 'space'				:x:

- toggle maximise window:		win + 'm'					:heavy_check_mark:
- maximise window:				win + 'j'					:heavy_check_mark:
- minimize window:				win + 'k'					:heavy_check_mark:
- window tile left:				win + 'h'					:heavy_check_mark:
- window tile right:			win + 'l'					:heavy_check_mark:
- window kill:					ctrl + shift + 'q'			:heavy_check_mark:

- switch workspace:				win + num					:heavy_check_mark:
- send window to workspace:		win + shift + num			:heavy_check_mark:

- toggle mute sound:			win + '0'					:heavy_check_mark:
- sound up:						win + '='					:heavy_check_mark:
- sound down:					win + '-'					:heavy_check_mark:

- show all window:				win + shift + 'space'		:x:
- lock session:					win + 'x'					:heavy_check_mark:
